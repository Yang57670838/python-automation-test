Feature: Login as valid user
    
    Scenario: Login as an existing user

        Given I am a user with a valid credentials
        When I login with my credentials
        Then I should see the text "Welcome" and my full name