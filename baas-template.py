import os
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

#chrome driver
from selenium.webdriver.chrome.service import Service

current_dir = os.path.dirname(__file__)
chrome_mac_driver_path = os.path.join(current_dir, "drivers/chromedriver")

service_obj = Service(chrome_mac_driver_path)
driver = webdriver.Chrome(service=service_obj)

driver.maximize_window()
driver.get("http://localhost:3000")
print(driver.title)
print(driver.current_url)

# TODO: page loading seems take time, check it??
time.sleep(5)
# login
driver.find_element(By.ID, "startFormBtn").click()

time.sleep(5)

# find web page header
pageHeader = driver.find_element(By.CLASS_NAME, "MuiTypography-h4").text
assert pageHeader == "header"

# fill in personal details
genderSelect = driver.find_element(By.ID, "gender")
genderSelect.click()
options = driver.find_elements(By.XPATH, "//ul[@aria-labelledby='gender-select-label']/li")
for option in options:
    if option.text == "Male":
        option.click()
        break

time.sleep(5)
driver.close()
