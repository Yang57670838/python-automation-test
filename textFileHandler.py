import os

# read
current_dir = os.path.dirname(__file__)
file_path = os.path.join(current_dir, "mock/test.txt")
with open(file_path, "r") as file:
    for line in file.readlines():
        print(line)