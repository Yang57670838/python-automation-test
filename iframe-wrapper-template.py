import os
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

#chrome driver
from selenium.webdriver.chrome.service import Service

current_dir = os.path.dirname(__file__)
chrome_mac_driver_path = os.path.join(current_dir, "drivers/chromedriver")

service_obj = Service(chrome_mac_driver_path)
driver = webdriver.Chrome(service=service_obj)

driver.maximize_window()
driver.get("http://localhost:3006")
print(driver.title)
print(driver.current_url)

# login

driver.find_element(By.CSS_SELECTOR, "input[name='login-email']").send_keys("jerry@gmail.com")
driver.find_element(By.ID, "login-password").send_keys("Jerry")
driver.find_element(By.XPATH, "//button[@aria-label='login']").click()



# forget password

# driver.find_element(By.LINK_TEXT, "Forget password?").click()
# time.sleep(1)
# driver.find_element(By.XPATH, "//form/div[1]/div/input[@name='reset-password-email']").send_keys("jerry@gmail.com")
# driver.find_element(By.CSS_SELECTOR, "form div:nth-child(3) div input[name='reset-password']").send_keys("random1")
# driver.find_element(By.CSS_SELECTOR, "form div:nth-child(4) div input[name='reset-password-confirm']").send_keys("random1")
# driver.find_element(By.ID, "updatePasswordBtn").click()


time.sleep(5)

driver.close()
